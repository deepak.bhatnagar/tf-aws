#!/bin/bash
set -ex
echo 'install terrafrom'
echo $PATH
terraform --version
echo '#######################################'
cd ARM-Templates
echo '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
ls
echo '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
cd Deploy-PHP
echo '&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&'
terraform init
echo '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
terraform validate
echo '*************************************'
terraform plan

