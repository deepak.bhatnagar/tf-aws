provider "aws" {
  region = var.aws_region
  access_key = var.access-key
  secret_key = var.secret-key
}
