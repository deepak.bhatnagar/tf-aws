
module "VPC" {
  source               = "../modules/TF-VPC"
  project_name         = var.project_name
  vpc_cidr             = "10.30.0.0/16"
  public_subnets_cidr  = ["10.30.1.0/24", "10.30.2.0/24"]
  private_subnets_cidr = ["10.30.3.0/24", "10.30.4.0/24"]
}


# module "EC2" {
#   source       = "../modules/TF-EC2"
#    vpc_id       =  "module.VPC.vpc_id"
#    subnet_id    =  "module.VPC.subnet_id"
#   project_name = var.project_name
# }
