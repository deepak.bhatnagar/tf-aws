resource "aws_vpc" "VPC" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = join("-", [var.project_name, "VPC"])
  }

}

# Internet Gateway
resource "aws_internet_gateway" "internet-gateway" {
  vpc_id = aws_vpc.VPC.id
  tags = {
    Name = join("-", [var.project_name, "internet-gateway"])
  }
}

# Define the nat eip
resource "aws_eip" "elastic-ip" {
  vpc = true

  tags = {
    Name = join("-", [var.project_name, "elastic-ip"])
  }
}

# Subnets : Public
resource "aws_subnet" "public-subnet" {
  count             = length(var.public_subnets_cidr)
  vpc_id            = aws_vpc.VPC.id
  cidr_block        = element(var.public_subnets_cidr, count.index)
  availability_zone = element(var.azs, count.index)
  tags = {
    Name = join("-", [var.project_name, "Public-Subnet-${count.index + 1}"])
  }
}

# Route table: attach Internet Gateway 
resource "aws_route_table" "public-route-table" {
  vpc_id = aws_vpc.VPC.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet-gateway.id
  }
  tags = {
    Name = join("-", [var.project_name, "public-RT"])
  }
}

# Route table association with public subnets
resource "aws_route_table_association" "public-rt-association" {
  count          = length(var.public_subnets_cidr)
  subnet_id      = element(aws_subnet.public-subnet.*.id, count.index)
  route_table_id = aws_route_table.public-route-table.id
}

# Define the nat gateway
resource "aws_nat_gateway" "nat-gateway" {
  allocation_id = aws_eip.elastic-ip.id
  count         = 1
  subnet_id     = element(aws_subnet.public-subnet.*.id, count.index)
  tags = {
    Name = join("-", [var.project_name, "NAT-gateway"])
  }
}



#Subnets : Private
resource "aws_subnet" "private-subnet" {
  count             = length(var.private_subnets_cidr)
  vpc_id            = aws_vpc.VPC.id
  cidr_block        = element(var.private_subnets_cidr, count.index)
  availability_zone = element(var.azs, count.index)
  tags = {
    Name = join("-", [var.project_name, "Private-Subnet-${count.index + 1}"])
  }
}

# Route table: attach Internet Gateway 
resource "aws_route_table" "private-route-table" {
  vpc_id = aws_vpc.VPC.id
  count  = 1
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = element(aws_nat_gateway.nat-gateway.*.id, count.index)
  }
  tags = {
    Name = join("-", [var.project_name, "private-RT"])
  }
}

# Route table association with private subnets
resource "aws_route_table_association" "private-rt-association" {
  count          = length(var.private_subnets_cidr)
  subnet_id      = element(aws_subnet.private-subnet.*.id, count.index)
  route_table_id = element(aws_route_table.private-route-table.*.id, count.index)
}