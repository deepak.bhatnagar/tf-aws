output "vpc_id" {
  value = aws_vpc.VPC.id
}

output "internet_gateway" {
  value = aws_internet_gateway.internet-gateway.id
}

output "eip_id" {
  value = aws_eip.elastic-ip.id
}

output "subnet_id" {
  value = aws_subnet.public-subnet.*.id
}









