resource "aws_security_group" "http_server_sg" {
  name   = "http_server_sg"
  vpc_id = module.VPC.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = join("-", [var.project_name, "security-group"])
  }
}

resource "aws_instance" "myInstanceAWS" {
  count                  = var.instance_count
  instance_type          = var.instance_type
  ami                    = var.ami_id
  key_name               = var.ssh_key_name
  subnet_id              = var.subnet_id
  vpc_security_group_ids = [aws_security_group.http_server_sg.id]
  tags = {
    Name = join("-", [var.project_name, "instance"])
  }
}

resource "aws_eip_association" "eip_assoc" {
  count                  = var.instance_count
  instance_id   =   element(aws_instance.myInstanceAWS.*.id, count.index)
  allocation_id =   element(aws_eip.elastic-ip.*.id, count.index)
  
}

resource "aws_eip" "elastic-ip" {
  count                  = var.instance_count
  vpc = true
  tags = {
    Name = join("-", [var.project_name, "elastic-ip"])
  }
}

