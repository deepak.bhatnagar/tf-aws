
variable "project_name" {
  default = "terra-arm"
}
variable "instance_type" {
  default = "t2.micro"
}
variable "instance_name" {
  default = "terra-ansible"
}
variable "ami_id" {
  default = "ami-026dea5602e368e96"
}
variable "ssh_user_name" {
  default = "ec2-user"
}
variable "ssh_key_name" {
  default = "terra-test"
}
variable "ssh_key_path" {
  default = "~/aws/aws_keys/terra-test.pem"
}
variable "instance_count" {
  default = 1
}
variable "subnet_id" {
  # default = "subnet-739eb109" 
}

 variable "vpc_id" {}



# variable "dev_host_label" {
#   default = "terra_ansible_host"
# }
